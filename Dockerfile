FROM python:3.7-alpine
RUN apk add --no-cache git gcc musl-dev
RUN git clone https://gitlab.com/Cardiox12/robocop.git /srv/robocop
WORKDIR /srv/robocop
RUN pip install -r requirements.txt
CMD ["python", "robocop.py"]
